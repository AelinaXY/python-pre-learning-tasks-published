def calculator(a, b, operator):
    # ==============
    # Your code here

    options = {
        '+': add,
        '-': sub,
        '*': mult,
        '/': div
        }
    
    return options[operator](a,b)

    # ==============

def add(a,b): return a+b

def sub(a,b): return a-b

def mult(a,b): return a*b

def div(a,b): return a/b

#def binaryConvert(a): return bin(int(a))[2:]

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
