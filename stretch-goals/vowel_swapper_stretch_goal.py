def vowel_swapper(string):
    # ==============
    # Your code here
    stringList = list(string)
    vowelCount = [0,0,0,0,0]
    vowelDict= {'a': 0, 'e': 1, 'i': 2, 'o': 3, 'u': 4, 'A': 0, 'E': 1, 'I': 2, 'O': 3, 'U': 4}

    for i in range(0, len(stringList)):
        if stringList[i] in vowelDict:

            vowelDictNum = vowelDict[stringList[i]]
           
            if vowelCount[vowelDictNum] == 0:
                vowelCount[vowelDictNum] += 1

            elif vowelCount[vowelDictNum] == 1:
                
                vowelCount[vowelDictNum] = vowelCount[vowelDictNum] + 1
                stringList[i] = stringList[i].replace("a", "4")
                stringList[i] = stringList[i].replace("A", "4")
                stringList[i] = stringList[i].replace("e", "3")
                stringList[i] = stringList[i].replace("E", "3")
                stringList[i] = stringList[i].replace("i", "!")
                stringList[i] = stringList[i].replace("I", "!")
                stringList[i] = stringList[i].replace("o", "ooo")
                stringList[i] = stringList[i].replace("O", "000")
                stringList[i] = stringList[i].replace("u", "|_|")
                stringList[i] = stringList[i].replace("U", "|_|")

    return ''.join(stringList)


        



    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
